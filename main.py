#!/usr/bin/env python3

# TODO refactor code (mais 1 fichier c'est pratique :p)
# TODO check state of containers, better docker handling while deployement
# TODO add commit-sha, link, pipeline link, etc.
# TODO add url
# TODO check concurrency
# TODO add a endpoint for monitoring


"""
 Made by Axel for Le Club Info
 Python3 please
"""
import json
import os
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
import ast
import docker  # dockumentation here : https://docker-py.readthedocs.io/en/stable/
import requests

DEFAULT_PORT = 8100

MNT_PATH="./mnt/"

def loadProjectDefinitions():
    file = open("projects.dict", "r")
    contents = file.read()
    dictionary = ast.literal_eval(contents)
    file.close()
    return dictionary


PROJECT_DEFINITIONS = loadProjectDefinitions()

IMG_NAME_INDEX = 0
PASSWORD_INDEX = 1
PORT_INDEX = 2
VOLUME_INDEX = 3

LOGS_MAX_LEN = 6000

def loadDiscohookUrl():
    file = open("discohook.url", "r")
    return file.read().strip()


DISCORD_HOOK = loadDiscohookUrl()

CBLUE = "5815551"
CRED = "15282224"
CGREEN = "3205498"  # not used yet, maybe if checking status of container after
CNONE = "0"  # hmm, debug or not planned

HSUCCESS = {"name": "C-est une réussite !",
            "icon_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/81/Emblem-important-blue.svg/48px-Emblem-important-blue.svg.png"}
HFAILURE = {"name": "Woops, échec ...",
            "icon_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Emblem-important-red.svg/48px-Emblem-important-red.svg.png"}

DOCKER = docker.from_env()

def tailString(strInput):
    if len(strInput) > LOGS_MAX_LEN:
        return strInput[-LOGS_MAX_LEN:]
    return strInput


def getEnvFile(slug):
    return "./mnt/" + slug + "/env"


class S(BaseHTTPRequestHandler):
    def _set_response(self, code):
        self.send_response(code)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def getConfigFromHeaders(self):
        # check if headers is set up
        if self.headerIsNotFull():
            logging.info("Request with no header")
            self._set_response(400)
            raise
        depengineSlug = self.headers.get("X-Depengine-Slug")
        depengineToken = self.headers.get("X-Depengine-Token")

        # check if slug exist in config
        if not depengineSlug in PROJECT_DEFINITIONS:  # is dict contain key value
            logging.info("Request with wrong header : no or wrong project ask")
            self._set_response(404)
            raise
        config = PROJECT_DEFINITIONS.get(depengineSlug)
        logging.info("using conf:" + str(config))

        # check token
        if depengineToken != config[PASSWORD_INDEX]:
            logging.error("Request with wrong header, wrong token for project : " + depengineSlug)
            self._set_response(401)
            raise

        return depengineSlug, config

    def do_GET(self): # For sending logs
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))

        try:
            depengineSlug, config = self.getConfigFromHeaders()
        except:
            return

        try:
            docker = DOCKER.containers.get(depengineSlug)
        except :
            self._set_response(404)
            return

        self._set_response(200)
        self.wfile.write(tailString(docker.logs()))

    def headerIsNotFull(self):
        return not ("X-Depengine-Slug" in self.headers and "X-Depengine-Token" in self.headers)

    def removeOldContainer(self, name):
        for container in DOCKER.containers.list(all=True):
            if (container.attrs["Name"] == "/" + name):
                logging.info("Removing old container")
                container.stop()
                container.remove()

    def loadEnvFile(self, slug):
        # check if env file
        if(os.path.isfile(getEnvFile(slug))):
            file = open(getEnvFile(slug), "r")
            output = file.read().splitlines()
            file.close()
            return output
        return []

    def dockerRoutine(self, slug, config):
        DOCKER.images.pull(config[IMG_NAME_INDEX])
        self.removeOldContainer(slug)
        DOCKER.containers.run(config[IMG_NAME_INDEX], detach=True, name=slug, ports=config[PORT_INDEX], volumes=config[VOLUME_INDEX], environment=self.loadEnvFile(slug))

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
        post_data = self.rfile.read(content_length)  # <--- Gets the data itself
        logging.debug("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                      str(self.path), str(self.headers), post_data.decode('utf-8'))

        try:
            depengineSlug, config = self.getConfigFromHeaders()
        except:
            return

        logging.info("Running a new deployement for : " + depengineSlug)

        try:
            self.dockerRoutine(depengineSlug, config)
        except BaseException as err:
            discoHook("Erreur, impossibilité de lancer le conteneur", depengineSlug, head=HFAILURE, color=CRED)
            logging.error("Docker error at run : " + str(err))
            self._set_response(500)
            return

        discoHook("Nouveau déploiement tout neuf chef !", depengineSlug, head=HSUCCESS, color=CBLUE)
        logging.info("Deployed : " + depengineSlug)
        self._set_response(200)


def discoHook(msg, project, head=None, color=CNONE):
    embed = {"description": msg,
             "title": project,
             "url": "http://club.plil.fr",
             "color": color,
             "author": head
             }
    data = {"username": "Depengine",
            "avatar_url": "https://discohook.org/static/discord-avatar.png",
            "content": None,
            "embeds": [embed]
            }
    requests.post(DISCORD_HOOK, data=json.dumps(data), headers={'content-type': 'application/json'})


def configLogger():
    logging.basicConfig(level=logging.INFO)

    logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    rootLogger = logging.getLogger()

    fileHandler = logging.FileHandler("log")
    fileHandler.setFormatter(logFormatter)
    rootLogger.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)


def run(server_class=HTTPServer, handler_class=S, port=DEFAULT_PORT):
    configLogger()
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting depengine...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping depengine...\n')


if __name__ == '__main__':
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
