# depengine

Moteur de déploiement spécialisé pour le club info avec Gitlab et Docker ! :)

# Installation

Vérifiez que vous êtes bien connecté aux registries docker.

Installez la routine cron présente dans le fichier crontab pour nettoyer le système le mercredi à 3h20 .

# Test

Le fichier testEndpoints.http pour le runner de pycharm.

# Utilisateur : ajouter un nouveau service en déploiement

La manipulation est obligatoirement faite par un sys-admin du Club Info, néamoins vous pouvez faciliter leur vie en leur indiquant quels dossiers doivent être péréniser (comme une base de donnée). Vous prendre connaissances des ports natifs auxquels sont affectués l'application ainsi que l'URL pour la version en production sur le port 80.

# Utilisateur : récupérer les logs

Vous pouvez facilement récupérer les logs avec : 

```shell
curl -H "X-Depengine-Slug: $DEPLOY_SLUG" -H "X-Depengine-Token: $DEPLOY_TOKEN" -s http://club.plil.fr:8100
```

# Administrateur

Le fichier de gestion du déploiement est projects.dict, il est documenté par le .example associé.

Il est possible d'ajouter un fichier de variable d'environnement en ajoutant un fichier %INSTALL_ROOT%/mnt/projectSlug/env .